// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.3 (lin64) Build 2405991 Thu Dec  6 23:36:41 MST 2018
// Date        : Mon Dec  2 19:12:51 2019
// Host        : optiplex-3020 running 64-bit Ubuntu 18.04.3 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ vdma_reloj_0_0_sim_netlist.v
// Design      : vdma_reloj_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z010clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_reloj
   (salida,
    clk);
  output salida;
  input clk;

  wire clk;
  wire [26:0]cont;
  wire cont0_carry__0_n_0;
  wire cont0_carry__0_n_1;
  wire cont0_carry__0_n_2;
  wire cont0_carry__0_n_3;
  wire cont0_carry__1_n_0;
  wire cont0_carry__1_n_1;
  wire cont0_carry__1_n_2;
  wire cont0_carry__1_n_3;
  wire cont0_carry__2_n_0;
  wire cont0_carry__2_n_1;
  wire cont0_carry__2_n_2;
  wire cont0_carry__2_n_3;
  wire cont0_carry__3_n_0;
  wire cont0_carry__3_n_1;
  wire cont0_carry__3_n_2;
  wire cont0_carry__3_n_3;
  wire cont0_carry__4_n_0;
  wire cont0_carry__4_n_1;
  wire cont0_carry__4_n_2;
  wire cont0_carry__4_n_3;
  wire cont0_carry__5_n_3;
  wire cont0_carry_n_0;
  wire cont0_carry_n_1;
  wire cont0_carry_n_2;
  wire cont0_carry_n_3;
  wire [0:0]cont_0;
  wire [26:1]data0;
  wire pulso;
  wire pulso_i_1_n_0;
  wire pulso_i_2_n_0;
  wire pulso_i_3_n_0;
  wire pulso_i_4_n_0;
  wire pulso_i_5_n_0;
  wire pulso_i_6_n_0;
  wire pulso_i_7_n_0;
  wire pulso_i_8_n_0;
  wire salida;
  wire [3:1]NLW_cont0_carry__5_CO_UNCONNECTED;
  wire [3:2]NLW_cont0_carry__5_O_UNCONNECTED;

  CARRY4 cont0_carry
       (.CI(1'b0),
        .CO({cont0_carry_n_0,cont0_carry_n_1,cont0_carry_n_2,cont0_carry_n_3}),
        .CYINIT(cont[0]),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[4:1]),
        .S(cont[4:1]));
  CARRY4 cont0_carry__0
       (.CI(cont0_carry_n_0),
        .CO({cont0_carry__0_n_0,cont0_carry__0_n_1,cont0_carry__0_n_2,cont0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[8:5]),
        .S(cont[8:5]));
  CARRY4 cont0_carry__1
       (.CI(cont0_carry__0_n_0),
        .CO({cont0_carry__1_n_0,cont0_carry__1_n_1,cont0_carry__1_n_2,cont0_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[12:9]),
        .S(cont[12:9]));
  CARRY4 cont0_carry__2
       (.CI(cont0_carry__1_n_0),
        .CO({cont0_carry__2_n_0,cont0_carry__2_n_1,cont0_carry__2_n_2,cont0_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[16:13]),
        .S(cont[16:13]));
  CARRY4 cont0_carry__3
       (.CI(cont0_carry__2_n_0),
        .CO({cont0_carry__3_n_0,cont0_carry__3_n_1,cont0_carry__3_n_2,cont0_carry__3_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[20:17]),
        .S(cont[20:17]));
  CARRY4 cont0_carry__4
       (.CI(cont0_carry__3_n_0),
        .CO({cont0_carry__4_n_0,cont0_carry__4_n_1,cont0_carry__4_n_2,cont0_carry__4_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[24:21]),
        .S(cont[24:21]));
  CARRY4 cont0_carry__5
       (.CI(cont0_carry__4_n_0),
        .CO({NLW_cont0_carry__5_CO_UNCONNECTED[3:1],cont0_carry__5_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_cont0_carry__5_O_UNCONNECTED[3:2],data0[26:25]}),
        .S({1'b0,1'b0,cont[26:25]}));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \cont[0]_i_1 
       (.I0(cont[0]),
        .O(cont_0));
  LUT3 #(
    .INIT(8'h01)) 
    \cont[26]_i_1 
       (.I0(pulso_i_2_n_0),
        .I1(pulso_i_3_n_0),
        .I2(pulso_i_4_n_0),
        .O(pulso));
  FDRE #(
    .INIT(1'b0)) 
    \cont_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(cont_0),
        .Q(cont[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \cont_reg[10] 
       (.C(clk),
        .CE(1'b1),
        .D(data0[10]),
        .Q(cont[10]),
        .R(pulso));
  FDRE #(
    .INIT(1'b0)) 
    \cont_reg[11] 
       (.C(clk),
        .CE(1'b1),
        .D(data0[11]),
        .Q(cont[11]),
        .R(pulso));
  FDRE #(
    .INIT(1'b0)) 
    \cont_reg[12] 
       (.C(clk),
        .CE(1'b1),
        .D(data0[12]),
        .Q(cont[12]),
        .R(pulso));
  FDRE #(
    .INIT(1'b0)) 
    \cont_reg[13] 
       (.C(clk),
        .CE(1'b1),
        .D(data0[13]),
        .Q(cont[13]),
        .R(pulso));
  FDRE #(
    .INIT(1'b0)) 
    \cont_reg[14] 
       (.C(clk),
        .CE(1'b1),
        .D(data0[14]),
        .Q(cont[14]),
        .R(pulso));
  FDRE #(
    .INIT(1'b0)) 
    \cont_reg[15] 
       (.C(clk),
        .CE(1'b1),
        .D(data0[15]),
        .Q(cont[15]),
        .R(pulso));
  FDRE #(
    .INIT(1'b0)) 
    \cont_reg[16] 
       (.C(clk),
        .CE(1'b1),
        .D(data0[16]),
        .Q(cont[16]),
        .R(pulso));
  FDRE #(
    .INIT(1'b0)) 
    \cont_reg[17] 
       (.C(clk),
        .CE(1'b1),
        .D(data0[17]),
        .Q(cont[17]),
        .R(pulso));
  FDRE #(
    .INIT(1'b0)) 
    \cont_reg[18] 
       (.C(clk),
        .CE(1'b1),
        .D(data0[18]),
        .Q(cont[18]),
        .R(pulso));
  FDRE #(
    .INIT(1'b0)) 
    \cont_reg[19] 
       (.C(clk),
        .CE(1'b1),
        .D(data0[19]),
        .Q(cont[19]),
        .R(pulso));
  FDRE #(
    .INIT(1'b0)) 
    \cont_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(data0[1]),
        .Q(cont[1]),
        .R(pulso));
  FDRE #(
    .INIT(1'b0)) 
    \cont_reg[20] 
       (.C(clk),
        .CE(1'b1),
        .D(data0[20]),
        .Q(cont[20]),
        .R(pulso));
  FDRE #(
    .INIT(1'b0)) 
    \cont_reg[21] 
       (.C(clk),
        .CE(1'b1),
        .D(data0[21]),
        .Q(cont[21]),
        .R(pulso));
  FDRE #(
    .INIT(1'b0)) 
    \cont_reg[22] 
       (.C(clk),
        .CE(1'b1),
        .D(data0[22]),
        .Q(cont[22]),
        .R(pulso));
  FDRE #(
    .INIT(1'b0)) 
    \cont_reg[23] 
       (.C(clk),
        .CE(1'b1),
        .D(data0[23]),
        .Q(cont[23]),
        .R(pulso));
  FDRE #(
    .INIT(1'b0)) 
    \cont_reg[24] 
       (.C(clk),
        .CE(1'b1),
        .D(data0[24]),
        .Q(cont[24]),
        .R(pulso));
  FDRE #(
    .INIT(1'b0)) 
    \cont_reg[25] 
       (.C(clk),
        .CE(1'b1),
        .D(data0[25]),
        .Q(cont[25]),
        .R(pulso));
  FDRE #(
    .INIT(1'b0)) 
    \cont_reg[26] 
       (.C(clk),
        .CE(1'b1),
        .D(data0[26]),
        .Q(cont[26]),
        .R(pulso));
  FDRE #(
    .INIT(1'b0)) 
    \cont_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(data0[2]),
        .Q(cont[2]),
        .R(pulso));
  FDRE #(
    .INIT(1'b0)) 
    \cont_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(data0[3]),
        .Q(cont[3]),
        .R(pulso));
  FDRE #(
    .INIT(1'b0)) 
    \cont_reg[4] 
       (.C(clk),
        .CE(1'b1),
        .D(data0[4]),
        .Q(cont[4]),
        .R(pulso));
  FDRE #(
    .INIT(1'b0)) 
    \cont_reg[5] 
       (.C(clk),
        .CE(1'b1),
        .D(data0[5]),
        .Q(cont[5]),
        .R(pulso));
  FDRE #(
    .INIT(1'b0)) 
    \cont_reg[6] 
       (.C(clk),
        .CE(1'b1),
        .D(data0[6]),
        .Q(cont[6]),
        .R(pulso));
  FDRE #(
    .INIT(1'b0)) 
    \cont_reg[7] 
       (.C(clk),
        .CE(1'b1),
        .D(data0[7]),
        .Q(cont[7]),
        .R(pulso));
  FDRE #(
    .INIT(1'b0)) 
    \cont_reg[8] 
       (.C(clk),
        .CE(1'b1),
        .D(data0[8]),
        .Q(cont[8]),
        .R(pulso));
  FDRE #(
    .INIT(1'b0)) 
    \cont_reg[9] 
       (.C(clk),
        .CE(1'b1),
        .D(data0[9]),
        .Q(cont[9]),
        .R(pulso));
  LUT4 #(
    .INIT(16'hFE01)) 
    pulso_i_1
       (.I0(pulso_i_2_n_0),
        .I1(pulso_i_3_n_0),
        .I2(pulso_i_4_n_0),
        .I3(salida),
        .O(pulso_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hFFFFFFDF)) 
    pulso_i_2
       (.I0(cont[25]),
        .I1(cont[26]),
        .I2(cont[0]),
        .I3(pulso_i_5_n_0),
        .I4(pulso_i_6_n_0),
        .O(pulso_i_2_n_0));
  LUT5 #(
    .INIT(32'hFFFFEFFF)) 
    pulso_i_3
       (.I0(cont[7]),
        .I1(cont[8]),
        .I2(cont[5]),
        .I3(cont[6]),
        .I4(pulso_i_7_n_0),
        .O(pulso_i_3_n_0));
  LUT5 #(
    .INIT(32'hFFFFBFFF)) 
    pulso_i_4
       (.I0(cont[16]),
        .I1(cont[15]),
        .I2(cont[13]),
        .I3(cont[14]),
        .I4(pulso_i_8_n_0),
        .O(pulso_i_4_n_0));
  LUT4 #(
    .INIT(16'hFF7F)) 
    pulso_i_5
       (.I0(cont[22]),
        .I1(cont[21]),
        .I2(cont[23]),
        .I3(cont[24]),
        .O(pulso_i_5_n_0));
  LUT4 #(
    .INIT(16'h7FFF)) 
    pulso_i_6
       (.I0(cont[2]),
        .I1(cont[1]),
        .I2(cont[4]),
        .I3(cont[3]),
        .O(pulso_i_6_n_0));
  LUT4 #(
    .INIT(16'hFFEF)) 
    pulso_i_7
       (.I0(cont[10]),
        .I1(cont[9]),
        .I2(cont[12]),
        .I3(cont[11]),
        .O(pulso_i_7_n_0));
  LUT4 #(
    .INIT(16'hDFFF)) 
    pulso_i_8
       (.I0(cont[17]),
        .I1(cont[18]),
        .I2(cont[20]),
        .I3(cont[19]),
        .O(pulso_i_8_n_0));
  FDRE pulso_reg
       (.C(clk),
        .CE(1'b1),
        .D(pulso_i_1_n_0),
        .Q(salida),
        .R(1'b0));
endmodule

(* CHECK_LICENSE_TYPE = "vdma_reloj_0_0,reloj,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* ip_definition_source = "module_ref" *) 
(* x_core_info = "reloj,Vivado 2018.3" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (clk,
    salida);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN vdma_processing_system7_0_2_FCLK_CLK0, INSERT_VIP 0" *) input clk;
  output salida;

  wire clk;
  wire salida;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_reloj U0
       (.clk(clk),
        .salida(salida));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
