-- Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2018.3 (lin64) Build 2405991 Thu Dec  6 23:36:41 MST 2018
-- Date        : Mon Dec  2 19:12:51 2019
-- Host        : optiplex-3020 running 64-bit Ubuntu 18.04.3 LTS
-- Command     : write_vhdl -force -mode synth_stub
--               /home/samuel/Documents/vga_generador/min_vdma.srcs/sources_1/bd/vdma/ip/vdma_reloj_0_0/vdma_reloj_0_0_stub.vhdl
-- Design      : vdma_reloj_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z010clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity vdma_reloj_0_0 is
  Port ( 
    clk : in STD_LOGIC;
    salida : out STD_LOGIC
  );

end vdma_reloj_0_0;

architecture stub of vdma_reloj_0_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "clk,salida";
attribute x_core_info : string;
attribute x_core_info of stub : architecture is "reloj,Vivado 2018.3";
begin
end;
