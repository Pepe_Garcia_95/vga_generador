-- Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2018.3 (lin64) Build 2405991 Thu Dec  6 23:36:41 MST 2018
-- Date        : Mon Dec  2 19:12:51 2019
-- Host        : optiplex-3020 running 64-bit Ubuntu 18.04.3 LTS
-- Command     : write_vhdl -force -mode funcsim
--               /home/samuel/Documents/vga_generador/min_vdma.srcs/sources_1/bd/vdma/ip/vdma_reloj_0_0/vdma_reloj_0_0_sim_netlist.vhdl
-- Design      : vdma_reloj_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z010clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity vdma_reloj_0_0_reloj is
  port (
    salida : out STD_LOGIC;
    clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of vdma_reloj_0_0_reloj : entity is "reloj";
end vdma_reloj_0_0_reloj;

architecture STRUCTURE of vdma_reloj_0_0_reloj is
  signal cont : STD_LOGIC_VECTOR ( 26 downto 0 );
  signal \cont0_carry__0_n_0\ : STD_LOGIC;
  signal \cont0_carry__0_n_1\ : STD_LOGIC;
  signal \cont0_carry__0_n_2\ : STD_LOGIC;
  signal \cont0_carry__0_n_3\ : STD_LOGIC;
  signal \cont0_carry__1_n_0\ : STD_LOGIC;
  signal \cont0_carry__1_n_1\ : STD_LOGIC;
  signal \cont0_carry__1_n_2\ : STD_LOGIC;
  signal \cont0_carry__1_n_3\ : STD_LOGIC;
  signal \cont0_carry__2_n_0\ : STD_LOGIC;
  signal \cont0_carry__2_n_1\ : STD_LOGIC;
  signal \cont0_carry__2_n_2\ : STD_LOGIC;
  signal \cont0_carry__2_n_3\ : STD_LOGIC;
  signal \cont0_carry__3_n_0\ : STD_LOGIC;
  signal \cont0_carry__3_n_1\ : STD_LOGIC;
  signal \cont0_carry__3_n_2\ : STD_LOGIC;
  signal \cont0_carry__3_n_3\ : STD_LOGIC;
  signal \cont0_carry__4_n_0\ : STD_LOGIC;
  signal \cont0_carry__4_n_1\ : STD_LOGIC;
  signal \cont0_carry__4_n_2\ : STD_LOGIC;
  signal \cont0_carry__4_n_3\ : STD_LOGIC;
  signal \cont0_carry__5_n_3\ : STD_LOGIC;
  signal cont0_carry_n_0 : STD_LOGIC;
  signal cont0_carry_n_1 : STD_LOGIC;
  signal cont0_carry_n_2 : STD_LOGIC;
  signal cont0_carry_n_3 : STD_LOGIC;
  signal cont_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal data0 : STD_LOGIC_VECTOR ( 26 downto 1 );
  signal pulso : STD_LOGIC;
  signal pulso_i_1_n_0 : STD_LOGIC;
  signal pulso_i_2_n_0 : STD_LOGIC;
  signal pulso_i_3_n_0 : STD_LOGIC;
  signal pulso_i_4_n_0 : STD_LOGIC;
  signal pulso_i_5_n_0 : STD_LOGIC;
  signal pulso_i_6_n_0 : STD_LOGIC;
  signal pulso_i_7_n_0 : STD_LOGIC;
  signal pulso_i_8_n_0 : STD_LOGIC;
  signal \^salida\ : STD_LOGIC;
  signal \NLW_cont0_carry__5_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_cont0_carry__5_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \cont[0]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of pulso_i_2 : label is "soft_lutpair0";
begin
  salida <= \^salida\;
cont0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => cont0_carry_n_0,
      CO(2) => cont0_carry_n_1,
      CO(1) => cont0_carry_n_2,
      CO(0) => cont0_carry_n_3,
      CYINIT => cont(0),
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(4 downto 1),
      S(3 downto 0) => cont(4 downto 1)
    );
\cont0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => cont0_carry_n_0,
      CO(3) => \cont0_carry__0_n_0\,
      CO(2) => \cont0_carry__0_n_1\,
      CO(1) => \cont0_carry__0_n_2\,
      CO(0) => \cont0_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(8 downto 5),
      S(3 downto 0) => cont(8 downto 5)
    );
\cont0_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cont0_carry__0_n_0\,
      CO(3) => \cont0_carry__1_n_0\,
      CO(2) => \cont0_carry__1_n_1\,
      CO(1) => \cont0_carry__1_n_2\,
      CO(0) => \cont0_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(12 downto 9),
      S(3 downto 0) => cont(12 downto 9)
    );
\cont0_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \cont0_carry__1_n_0\,
      CO(3) => \cont0_carry__2_n_0\,
      CO(2) => \cont0_carry__2_n_1\,
      CO(1) => \cont0_carry__2_n_2\,
      CO(0) => \cont0_carry__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(16 downto 13),
      S(3 downto 0) => cont(16 downto 13)
    );
\cont0_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \cont0_carry__2_n_0\,
      CO(3) => \cont0_carry__3_n_0\,
      CO(2) => \cont0_carry__3_n_1\,
      CO(1) => \cont0_carry__3_n_2\,
      CO(0) => \cont0_carry__3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(20 downto 17),
      S(3 downto 0) => cont(20 downto 17)
    );
\cont0_carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \cont0_carry__3_n_0\,
      CO(3) => \cont0_carry__4_n_0\,
      CO(2) => \cont0_carry__4_n_1\,
      CO(1) => \cont0_carry__4_n_2\,
      CO(0) => \cont0_carry__4_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(24 downto 21),
      S(3 downto 0) => cont(24 downto 21)
    );
\cont0_carry__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \cont0_carry__4_n_0\,
      CO(3 downto 1) => \NLW_cont0_carry__5_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \cont0_carry__5_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 2) => \NLW_cont0_carry__5_O_UNCONNECTED\(3 downto 2),
      O(1 downto 0) => data0(26 downto 25),
      S(3 downto 2) => B"00",
      S(1 downto 0) => cont(26 downto 25)
    );
\cont[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cont(0),
      O => cont_0(0)
    );
\cont[26]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => pulso_i_2_n_0,
      I1 => pulso_i_3_n_0,
      I2 => pulso_i_4_n_0,
      O => pulso
    );
\cont_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => cont_0(0),
      Q => cont(0),
      R => '0'
    );
\cont_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => data0(10),
      Q => cont(10),
      R => pulso
    );
\cont_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => data0(11),
      Q => cont(11),
      R => pulso
    );
\cont_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => data0(12),
      Q => cont(12),
      R => pulso
    );
\cont_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => data0(13),
      Q => cont(13),
      R => pulso
    );
\cont_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => data0(14),
      Q => cont(14),
      R => pulso
    );
\cont_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => data0(15),
      Q => cont(15),
      R => pulso
    );
\cont_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => data0(16),
      Q => cont(16),
      R => pulso
    );
\cont_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => data0(17),
      Q => cont(17),
      R => pulso
    );
\cont_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => data0(18),
      Q => cont(18),
      R => pulso
    );
\cont_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => data0(19),
      Q => cont(19),
      R => pulso
    );
\cont_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => data0(1),
      Q => cont(1),
      R => pulso
    );
\cont_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => data0(20),
      Q => cont(20),
      R => pulso
    );
\cont_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => data0(21),
      Q => cont(21),
      R => pulso
    );
\cont_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => data0(22),
      Q => cont(22),
      R => pulso
    );
\cont_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => data0(23),
      Q => cont(23),
      R => pulso
    );
\cont_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => data0(24),
      Q => cont(24),
      R => pulso
    );
\cont_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => data0(25),
      Q => cont(25),
      R => pulso
    );
\cont_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => data0(26),
      Q => cont(26),
      R => pulso
    );
\cont_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => data0(2),
      Q => cont(2),
      R => pulso
    );
\cont_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => data0(3),
      Q => cont(3),
      R => pulso
    );
\cont_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => data0(4),
      Q => cont(4),
      R => pulso
    );
\cont_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => data0(5),
      Q => cont(5),
      R => pulso
    );
\cont_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => data0(6),
      Q => cont(6),
      R => pulso
    );
\cont_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => data0(7),
      Q => cont(7),
      R => pulso
    );
\cont_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => data0(8),
      Q => cont(8),
      R => pulso
    );
\cont_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => data0(9),
      Q => cont(9),
      R => pulso
    );
pulso_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FE01"
    )
        port map (
      I0 => pulso_i_2_n_0,
      I1 => pulso_i_3_n_0,
      I2 => pulso_i_4_n_0,
      I3 => \^salida\,
      O => pulso_i_1_n_0
    );
pulso_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFDF"
    )
        port map (
      I0 => cont(25),
      I1 => cont(26),
      I2 => cont(0),
      I3 => pulso_i_5_n_0,
      I4 => pulso_i_6_n_0,
      O => pulso_i_2_n_0
    );
pulso_i_3: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFEFFF"
    )
        port map (
      I0 => cont(7),
      I1 => cont(8),
      I2 => cont(5),
      I3 => cont(6),
      I4 => pulso_i_7_n_0,
      O => pulso_i_3_n_0
    );
pulso_i_4: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFBFFF"
    )
        port map (
      I0 => cont(16),
      I1 => cont(15),
      I2 => cont(13),
      I3 => cont(14),
      I4 => pulso_i_8_n_0,
      O => pulso_i_4_n_0
    );
pulso_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF7F"
    )
        port map (
      I0 => cont(22),
      I1 => cont(21),
      I2 => cont(23),
      I3 => cont(24),
      O => pulso_i_5_n_0
    );
pulso_i_6: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => cont(2),
      I1 => cont(1),
      I2 => cont(4),
      I3 => cont(3),
      O => pulso_i_6_n_0
    );
pulso_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFEF"
    )
        port map (
      I0 => cont(10),
      I1 => cont(9),
      I2 => cont(12),
      I3 => cont(11),
      O => pulso_i_7_n_0
    );
pulso_i_8: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DFFF"
    )
        port map (
      I0 => cont(17),
      I1 => cont(18),
      I2 => cont(20),
      I3 => cont(19),
      O => pulso_i_8_n_0
    );
pulso_reg: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => pulso_i_1_n_0,
      Q => \^salida\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity vdma_reloj_0_0 is
  port (
    clk : in STD_LOGIC;
    salida : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of vdma_reloj_0_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of vdma_reloj_0_0 : entity is "vdma_reloj_0_0,reloj,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of vdma_reloj_0_0 : entity is "yes";
  attribute ip_definition_source : string;
  attribute ip_definition_source of vdma_reloj_0_0 : entity is "module_ref";
  attribute x_core_info : string;
  attribute x_core_info of vdma_reloj_0_0 : entity is "reloj,Vivado 2018.3";
end vdma_reloj_0_0;

architecture STRUCTURE of vdma_reloj_0_0 is
  attribute x_interface_info : string;
  attribute x_interface_info of clk : signal is "xilinx.com:signal:clock:1.0 clk CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of clk : signal is "XIL_INTERFACENAME clk, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN vdma_processing_system7_0_2_FCLK_CLK0, INSERT_VIP 0";
begin
U0: entity work.vdma_reloj_0_0_reloj
     port map (
      clk => clk,
      salida => salida
    );
end STRUCTURE;
