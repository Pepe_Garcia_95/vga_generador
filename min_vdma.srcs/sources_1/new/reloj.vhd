library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity reloj is
Port(
	 clk: in std_logic;
	 salida: out std_logic
);

end reloj; 

architecture Behavioral of reloj is

Signal pulso: STD_LOGIC; ---reloj de 1 hz
Signal cont: integer range 0 to 99999999:=0;


begin
-- Pulso de reloj aproximadamente 1 Hz
	process(clk)
	begin
		if rising_edge(clk) then 
			if(cont=49999999) then 
				cont <= 0;
				pulso <= NOT(pulso);
			else
				cont <= cont+1;
			end if;
		end if;
	end process;
	
	salida<= pulso;


	
end Behavioral;